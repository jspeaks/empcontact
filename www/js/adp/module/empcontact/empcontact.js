define(["backbone", "jquery", "underscore",
    "adp/instance",
    "adp/component/login",
    "adp/component/logout",
    "adp/component/app"
], function(Backbone, $, _,
    instance,
    login,
    logout,
    app
){
    $(function(){
        instance.router = new (Backbone.Router.extend({
                routes:{
                    "login":"login",
                    "logout":"logout",
                    "*path":"app"
                },
                login:login,
                logout:logout,
                app:app
            }));
        Backbone.history.start({pushState: true});
    })
});
