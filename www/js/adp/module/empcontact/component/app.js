define(["jquery", "underscore",
    "adp/instance",
    "text!./template/app.html"
], function($, _,
    instance,
    template
){
    return function(){
        var $el = $("#app");

        function render(){
            $.get("/api/employees")
                .then(function(employees){
                    $el.html(_.template(template)({
                        employees:employees
                    }));

                    $el.find(".new").click(function(){
                        $(this).hide();
                        $(".newEmployee").show();
                        $(".newEmployee").find(".addEmployer").click(function(e){
                            e.preventDefault();
                            $(".newEmployee").find("input[name='employer']").last().clone().val("").insertBefore($(this));
                        });
                    });

                    $el.find(".newEmployee").submit(function(e){
                        var newEmployeeForm = $(this);
                        e.preventDefault();
                        $.post('/api/employee', newEmployeeForm.serialize()).then(function(){
                            render();
                        });
                    });

                    $el.find(".delete").click(function(e){
                        e.preventDefault();

                        var deleteButton = $(this),
                            employeeId = deleteButton.attr("data-id");

                        if(confirm("Are you sure you want to delete?")){
                            $.ajax({
                                url:"/api/employee/" + employeeId,
                                type:"DELETE"
                            }).then(function(data){
                                $el.find(".employee[data-id=" + employeeId+"]").remove();
                            });
                        }
                    });

                    $el.find(".edit").click(function(e){
                        e.preventDefault();

                        var employeeId = $(this).attr("data-id"),
                            employeePanel = $el.find(".employee[data-id=" + employeeId+"]");

                        employeePanel.find(".prop").each(function(){
                            $(this).replaceWith($("<input>", {
                                type:"text",
                                name:$(this).attr("data-prop"),
                                value:$(this).text()
                            }));
                        });

                        employeePanel.find(".addEntity").show().click(function(e){
                            e.preventDefault();
                            employeePanel.find(".addEntity").next("input[type='text']").clone().val("").insertAfter($(this)).focus();
                        });

                        employeePanel.find(".delete").hide();
                        employeePanel.find(".edit").hide();
                        employeePanel.find(".save").show();

                        employeePanel.find("form").submit(function(e){
                            e.preventDefault();

                            $.ajax({
                                url:"/api/employee/" + employeeId,
                                type:"PUT",
                                data:employeePanel.find("form").serialize()
                            }).then(function(data){
                                render();
                            });
                        });
                    });
                })
                .fail(function(){
                    $el.html("error");
                });
        }
        render();

        return {};
    };
});
