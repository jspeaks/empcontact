define(["jquery", "underscore",
    "adp/instance",
    "text!./template/login.html"
], function($, _,
    instance,
    template
){
    return function(){
        var $el = $("#app");
        $el.html(_.template(template)({}));

        var form = $el.find("form");

        form.submit(function(e){
            e.preventDefault();
            $.post('api/login',{
                pw:$el.find(".pw").val()
            }).then(function(data){
                if(data.loggedIn){
                    instance.router.navigate('', {trigger:true});
                }else{
                    form.addClass("error");
                }
            });
        });

        return {};
    };
});
