define(["jquery", "underscore",
    "text!./template/logout.html"
], function($, _,
    template
){
    return function(){
        $.post('api/logout').then(function(){
            $("#app").html(_.template(template)({}));
        });
        return {};
    };
});
