require.config({
    paths:{
		text:"//cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min",
        jquery:"//code.jquery.com/jquery-1.11.3.min",
        underscore:"//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min",
        backbone:"//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.2.3/backbone-min",
        adp:"./"
    },
    shim: {
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        }
    }
});

requirejs(["empcontact"]);