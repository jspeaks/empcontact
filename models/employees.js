var mongodb = require('mongodb'),
    _ = require('underscore'),
    Q = require('q'),
    uuid = require('node-uuid');

module.exports = function(params){
    var database = null,
        employeeCollection = null;

    return {
        connect:function(){
            if(database){
                return Q(database);
            }

            var connectPromise = Q.nfcall(mongodb.MongoClient.connect, params.uri);
            return connectPromise.then(function(db){
                database = db;
                employeeCollection = db.collection("employees");

                return db;
            });
        },
        all:function(){
            return Q.ninvoke(employeeCollection, "find", {}, {_id:0}).then(function(cursor){
                return Q.ninvoke(cursor, "toArray");
            });
        },
        get:function(id){
            return Q.ninvoke(employeeCollection, "find", {id:id}, {_id:0}).then(function(cursor){
                return cursor.next();
            });
        },
        new:function(employeeDetails){
            var employeeRecord = _.extend({}, employeeDetails, {id:uuid.v1()});
            return Q.ninvoke(employeeCollection, "insert", employeeRecord).then(function(){
                return _.omit(employeeRecord, "_id");
            });
        },
        update:function(id, employeeDetails){
            var employeeRecord = _.omit(employeeDetails, "id");
            return Q.ninvoke(employeeCollection, "update", {id:id}, {$set:employeeRecord})
                .then(function(){
                    return Q.ninvoke(employeeCollection, "find", {id:id}, {_id:0}).then(function(cursor){
                        return cursor.next();
                    });
                });
        },
        remove:function(id){
            return Q.ninvoke(employeeCollection, "remove", {id:id})
                .then(function(cursor){
                    return cursor.result;
                });
        }
    };
};