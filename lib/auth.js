module.exports = {
	authorized:function(req, res, next){
        if(!req.session || !req.session.authenticated){
            res.redirect('/login');
            return;
        }

        next();
	}
}