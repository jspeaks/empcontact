var express = require('express'),
	app = express(),
    session = require('express-session'),
    www = __dirname + '/www',
    router = express.Router(),
    authRoute = require('./routes/auth')(www),
    appRoute = require('./routes/app')(www),
    employeesRoute = require('./routes/employees');

app.use(session({
    secret:'058dcfa0cb7944baa04788a7fa1f7105',
    resave:false,
    saveUninitialized:true
}));

app.use('/', appRoute);
app.use(express.static(www));
app.use('/api', authRoute);
app.use('/api', employeesRoute);

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('ServerUp http://%s:%s', host, port);
});
