var express = require('express'),
    router = express.Router(),
    pw = process.env.ACCESS_PW || "adp",
    bodyParser = require('body-parser'),
    urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function(www){
    router.post('/login', urlencodedParser, function(req, res){
        var validLogin = (req.body.pw===pw);

        req.session.authenticated = validLogin;

        res.json({
            loggedIn:validLogin
        });
    });

    router.post('/logout', function(req, res){
        req.session.authenticated = false;

        res.json({
            loggedIn:false
        });
    });

    return router;
}