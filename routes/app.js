var express = require('express'),
    router = express.Router(),
    auth = require('../lib/auth');

module.exports = function(www){
    router.use('/login', express.static(www + "/index.html") );
    router.use('/logout', express.static(www + "/index.html") );
    router.get('/', auth.authorized, function(req, res, next){
        res.sendFile(www + "/index.html");
    });

    return router;
}