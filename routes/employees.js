var express = require('express'),
    router = express.Router(),
    employeesModel = require('../models/employees')({
        uri: process.env.MONGO_URI ||"mongodb://localhost/adpemployees"
    }),
    bodyParser = require('body-parser'),
    jsonParser = bodyParser.json(),
    urlencodedParser = bodyParser.urlencoded({ extended: false });

router.use(function(req, res, next){
    if(!req.session || !req.session.authenticated){
        res.status(401).json({authorized:false});
    }

    next();
});

router.use(function(req, res, next){
    employeesModel.connect()
        .then(function(){
            next();
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

router.get('/employees', function(req, res){
    employeesModel.all()
        .then(function(employees){
            res.json(employees);
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

router.post('/employee', urlencodedParser, function(req, res){
    employeesModel.new(req.body)
        .then(function(employeeRecord){
            res.json(employeeRecord);
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

router.get('/employee/:id', function(req, res){
    employeesModel.get(req.params.id)
        .then(function(employeeRecord){
            if(employeeRecord){
                res.json(employeeRecord);
            }else{
                res.status(404).json({
                    found:false
                });
            }
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

router.put('/employee/:id', urlencodedParser, function(req, res){
//router.put('/employee/:id', jsonParser, function(req, res){
    employeesModel.update(req.params.id, req.body)
        .then(function(employeeRecord){
            res.json(employeeRecord);
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

router.delete('/employee/:id', function(req, res){
    employeesModel.remove(req.params.id)
        .then(function(result){
            if(result.n){
                res.json({
                    removed:true
                });
            }else{
                res.status(404).json({
                    found:false
                });
            }
        })
        .catch(function(err){
            res.status(500).json(err);
        });
});

module.exports = router;